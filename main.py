#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

from headers import *

def main():
    k = aiml.Kernel()
    k.learn("regras.aiml")
    DatingSimNatural.learn(k)
    k.respond("Hi")
    while True:
        pergunta = raw_input("&> ")
        resposta = k.respond(pergunta)
        
        if(len(resposta) == 0):
			continue;
        
        chave = resposta.split()[0]
        use_cache = True
        
        if(chave[0] == "%"):
			use_cache = False
			chave = chave[1:]
			resposta = resposta[1:]
        
        if pergunta in memory:
            print memory[pergunta]
        elif chave in providers:
			r = providers[chave](pergunta, resposta)
			
			if(use_cache):
				memory[pergunta] = r
			
			print r
        else:
            print resposta
            
    return 0

if __name__ == '__main__':
    main()

