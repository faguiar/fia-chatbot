#!/usr/bin/python
# -*- coding: koi8-r -*-
"""
Pra conversar com esse bot voce precisa:
1. Criar uma conta em algum servidor xmpp (como blah.im, jabber.org ou jabber-br.org)
2. Adicionar o contato do robô como amigo por meio de um cliente (empathy, pidgin...)
3. Logar com o usuario e senha do robô para aceitar o convite de amizade
4. Deslogar o usuario do robô e rordar este programa

"""

JABBER_USER = "marvin_ufpel@blah.im"
PASSWORD = "demorgan"

# pacote python-xmpp em sistemas Red Hat e Debian-based
import xmpp

from headers import *

class JabberClient:
    def __init__(self, email, password):
        self.email = email
        self.password = password

        self.jid = xmpp.JID(email)

        self.user = self.jid.getNode()
        self.server = self.jid.getDomain()

        self.conn = xmpp.Client(self.server)
        conres = self.conn.connect()

        if not conres:
            print "Erro ao conectar-se ao server %s" % self.server
            print "O server pode estar offline ou voce pode estar desconectado"
            sys.exit(1)
        if conres <> 'tls':
            print "Nao foi possivel estabelecer uma conexão segura, continuando..."

        authres = self.conn.auth(self.user, self.password)
        if not authres:
            print "Erro de autenticação, usuário e/ou senha errados." % self.server
            sys.exit(1)
        if authres <> 'sasl':
            print "Não foi possível usar a autenticação SASL com %s. Utilizando autenticação simples..." % self.server

        # AIML
        self.k = aiml.Kernel()
        self.k.learn("regras.aiml")
        DatingSimNatural.learn(self.k)
        self.k.respond("Hi")

        self.conn.RegisterHandler('message', self.messageCB)
        self.conn.sendInitPresence()
        print "Bot started."
        self.GoOn()
        
    def messageCB(self, conn, mess):
        pergunta = mess.getBody()
        user = mess.getFrom()
       
        resposta = self.k.respond(pergunta)
        
        if(len(resposta) == 0):
			return;
        
        chave = resposta.split()[0]
        use_cache = True
        
        if(chave[0] == "%"):
			use_cache = False
			chave = chave[1:]
			resposta = resposta[1:]
        
        if pergunta in memory:
            self.conn.send(xmpp.Message(user, memory[pergunta]))
        elif chave in providers:
			r = providers[chave](pergunta, resposta)
			
			if(use_cache):
				memory[pergunta] = r
			
			self.conn.send(xmpp.Message(user, r))
        else:
            self.conn.send(xmpp.Message(user, resposta))

    def send_message(self, destination, message):
        self.conn.send(xmpp.Message(user, text))

    def StepOn(self):
        try:
            self.conn.Process(1)
        except KeyboardInterrupt: return 0
        return 1

    def GoOn(self):
        while self.StepOn(): pass

def main():
    jc = JabberClient(JABBER_USER, PASSWORD)
    
    return 0

if __name__ == '__main__':
    main()
