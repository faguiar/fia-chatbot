import random
import os

class DatingSim(object):
	_name = None
	_user = None
	
	knows_name = False;
	_shyness = 0.5
	_rudeness = 0.5
	
	_score = 0.0;
	
	def __init__(self):
		names = open(os.path.join(os.path.dirname(__file__),"names.txt")).read().strip().split()
		self._name = names[random.randint(0,len(names)-1)]
		
		self._shyness = random.random()
		self._rudeness = random.random()
		self._score = 0.0
	
	def ask_name(self):
		if(self._user == None and not self.knows_name):
			if(self._rudeness > 0.7 and self._shyness < 0.4):
				self._score -= 0.02
				return "Voce nao devia se apresentar antes?"
			else:
				self._score -= 0.01
		
		try:
			if(self.knows_name):
				if(self._shyness > 0.7):
					self._score -=0.02
					return "... %s... >.<" % (self._name,)
				elif(self._rudeness > 0.5):
					self._score -=0.02
					return "Eu ja te falei, nao?"
				elif(self._rudeness < 0.2):
					self._score -=0.01
					return "Haha, seu bobo. Voce sabe que eh %s." % (self._name,)
				else:
					self._score -=0.01
					return "%s. ^^\"" % (self._name,)
			else:
				if(self._shyness > 0.7):
					self._score +=0.015
					return "... %s." % (self._name,)
				elif(self._rudeness > 0.8):
					self._score +=0.005
					return "%s. Por que?" % (self._name,)
				elif(self._rudeness > 0.5):
					self._score +=0.01
					return "%s." % (self._name,)
				else:
					self._score +=0.01
					return "Eh %s." % (self._name,)
		finally:
			self.knows_name = True
	
	def introduce(self,user):
		if(self._user == None):
			self._user = user
			if(self._shyness > 0.7):
				self._score +=0.01
				return "O-ola %s..." % (self._user,)
			elif(self._shyness > 0.5):
				self._score +=0.01
				return "Ola %s..." % (self._user,)
			elif(self._rudeness > 0.8):
				self._score +=0.005
				return "Hmmm."
			else:
				self._score +=0.01
				return "Prazer em conhecer."
		elif(self._user == user):
			if(self._shyness > 0.7):
				self._score -=0.001
				return "Sim... Ola %s..." % (self._user,)
			elif(self._rudeness > 0.5):
				self._score -=0.002
				return "Sim, voce ja me disse isso..."
			elif(self._shyness > 0.5):
				self._score -=0.001
				return "Oi..."
			else:
				self._score -=0.001
				return "Sim, hehehe."
		else:
			try:
				if(self._shyness > 0.7 and self._rudeness < 0.3):
					self._score -=0.015
					return "Hehe, achei que fosse %s. ^^" % (self._user,)
				elif(self._shyness > 0.7):
					self._score -=0.01
					return "Achei que fosse %s..." % (self._user,)
				elif(self._rudeness > 0.7):
					self._score -=0.03
					return "%s, %s. Tanto faz." % (user, self._user,)
				else:
					self._score -=0.01
					return "Nao era %s?" % (self._user,)
			finally:
				self._user = user;
	
	def tudo_bem(self):
		if(self._score > 0):
			return "Tudo bem sim..."
		else:
			return "Eh, Tudo bem..."
