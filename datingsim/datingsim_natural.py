from datingsim import DatingSim

import os
import string

class DatingSimNatural(DatingSim):
	__INSTANCE = None
	
	@staticmethod
	def get_instance():
		if(DatingSimNatural.__INSTANCE == None):
			DatingSimNatural.__INSTANCE = DatingSimNatural()
		return DatingSimNatural.__INSTANCE
	
	@staticmethod
	def answer(pergunta,resposta):
		d = DatingSimNatural.get_instance()
		
		chave = resposta.split()[0]
		
		if(chave == "$dating_ask_name$"):
			return d.ask_name()
		elif(chave == "$dating_tudo_bem$"):
			return d.tudo_bem()
		elif(chave == "$dating_introduce$"):
			name = ' '.join(resposta.strip().split()[1:])
			name = string.capwords(name)
			return d.introduce(name)
	
	@staticmethod
	def learn(aiml):
		aiml.learn(os.path.join(os.path.dirname(__file__),"dating_rules.aiml"))
	
	@staticmethod
	def providers():
		return {
			"$dating_ask_name$": DatingSimNatural.answer,
			"$dating_introduce$": DatingSimNatural.answer,
			"$dating_tudo_bem$": DatingSimNatural.answer,
		}
	
	def parse(s):
		print s
	
