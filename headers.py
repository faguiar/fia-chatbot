#!/usr/bin/env python
# -*- coding: utf-8 -*-
#

import aiml

from imdb_provider import IMDB
from wiki_provider import WIKIPEDIA
from math_provider import MATH
from datetime_provider import DATETIME
from datingsim.datingsim_natural import DatingSimNatural

imdb = IMDB()
wiki = WIKIPEDIA()
math = MATH()
datetime = DATETIME()

providers = {}

providers['$imdb_movie_director$'] = imdb.answer
providers['$imdb_movies_of$'] = imdb.answer
providers['$imdb_is_in_movie$'] = imdb.answer
providers['$wiki_celeb$'] = wiki.answer
providers['$wiki_fact$'] = wiki.answer
providers['$math_question$'] = math.answer
providers['$datetime_today$'] = datetime.answer

providers.update(DatingSimNatural.providers())

memory = {}
