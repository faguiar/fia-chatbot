# -*- coding: utf-8 -*-
# http://imdbpy.sourceforge.net/
from imdb import IMDb

class IMDB:
    def __init__(self):
        self.imdb = IMDb()

    def answer(self, pergunta, resposta): 
        if resposta == "$imdb_movie_director$":
            return self.movie_director(pergunta)
        if resposta == "$imdb_movies_of$":
            return self.movies_of(pergunta)
        if resposta == "$imdb_is_in_movie$":
            return self.is_in_movie(pergunta)

    def movie_director(self, pergunta):
        movie = pergunta.rsplit(' ', 1)[1]
        s_result = self.imdb.search_movie(movie)
        movie_id = s_result[0].movieID
        movie_obj = self.imdb.get_movie(movie_id)

        return movie_obj['director'][0]['name']

    def movies_of(self, pergunta):
        person = pergunta[35:]
        sp = self.imdb.search_person(person)
        pid = sp[0].personID
        p = self.imdb.get_person(pid)

        movies_list = p.get('director movie')
        resposta = ""
        for movie in movies_list:
            resposta += movie['title'] + ", "

        return resposta
        
    def is_in_movie(self, pergunta):
        pergunta = pergunta.replace(" esta em ", ",")
        pergunta = pergunta[7:]
        keywords = pergunta.rsplit(',', 1)
        person = keywords[0]
        movie = keywords[1]
        
        pid = self.imdb.search_person(person)[0].personID
        p = self.imdb.get_person(pid)

        sid = self.imdb.search_movie(movie)[0].movieID

        movies_list = p.get('actor') or p.get('actress')
        for film in movies_list:
            if film.movieID == sid:
                return "Sim"

        return "Nao"
        
#i = IMDB()
#print i.answer('Brad Pitt esta em Inglourious Basterds?', '$imdb_is_in_movie$')
