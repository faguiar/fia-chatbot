#https://pypi.python.org/pypi/wikipedia/
import wikipedia

class WIKIPEDIA:
	def __init__(self):
		wikipedia.set_lang("pt")

	
	def answer(self, pergunta, resposta):
		if '$wiki_celeb$' in resposta:
			return self.celebrity(pergunta, resposta.replace('$wiki_celeb$ ',''))
		if '$wiki_fact$' in resposta:
			return self.fact(pergunta, resposta.replace('$wiki_fact$ ', ''))
			
	def celebrity(self, pergunta, query):			
		if query:
			ans = wikipedia.search(query)
			try: 
				summary = wikipedia.summary(str(query), sentences=1)
			except:
				return "Essa informacao nao consta no meu banco de dados (ou cerebro, como os humanos os chamam)..."
				
			qp = wikipedia.page(ans[0])
			
			if qp:
				return "Claro que eu sei quem eh. " + summary
			else:
				return "Nao sei do que voce esta falando... Poderias me contar mais sobre isso?"
			
		else:
			return 'Desculpe, mas nao fui programado para isso... Estou muito arrependido de ser tao burro...'
	
	def fact(self, pergunta, query):
		if query:
			ans = wikipedia.search(query)
			try: 
				summary = wikipedia.summary(str(query), sentences=1)
			except:
				return "Essa informacao nao consta no meu banco de dados (ou cerebro, como os humanos os chamam)..."
			qp = wikipedia.page(ans[0])
		if qp:
			return "Hm... " + summary 
		else:
			return 'Desculpe, mas nao fui programado para isso... Estou muito arrependido de ser tao burro...'
				
