# -*- coding: utf-8 -*-

import datetime

months = ["janeiro", "fevereiro", "março", "abril", "maio", "junho", "julho", "agosto", "setembro", "outubro", "novembro", "dezembro"]

class DATETIME:
    def __init__(self):
        self.datetime = datetime.datetime

    def answer(self, pergunta, resposta):
        if resposta == "$datetime_today$":
            return self.today()

    def today(self):
        now = self.datetime.now()
        return "Hoje eh dia %d de %s de %d" % (now.day, months[now.month-1], now.year)
        

#dt = DATETIME()
#print dt.answer("Que dia eh hoje?", "$datetime_today$")
    
